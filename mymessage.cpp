// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2015-07-09 00:55:16 (jmiller)>

#include <vector>
#include "mymessage.decl.h"

class VarSizeMsg : public CMessage_VarSizeMsg {
public:
  double* data;
  int size;
  VarSizeMsg(const std::vector<double>& v);
  void fillVector(std::vector<double>& v) const;
};

class Driver : public CBase_Driver {
public:
  Driver(CkArgMsg* msg);
  void run();
};

class TestChare : public CBase_TestChare {
public:
  TestChare(int size);
  void getData(CkFuture f);
private:
  std::vector<double> myData;
};

VarSizeMsg::VarSizeMsg(const std::vector<double>& v) {
  size = v.size();
  for (int i =0; i < size; ++i) {
    data[i] = v[i];
  }
}

void VarSizeMsg::fillVector(std::vector<double>& v) const {
  v.resize(size);
  for (int i = 0; i < size; ++i) {
    v[i] = data[i];
  }
}

TestChare::TestChare(int size) {
  myData.resize(size);
  for (int i = 0; i < size; ++i) {
    myData[i] = i*i;
  }
}

void TestChare::getData(CkFuture f) {
  VarSizeMsg* m = new (myData.size()) VarSizeMsg(myData);
  CkSendToFuture(f,m);
}

void Driver::run() {
  int msize = 5;
  std::vector<double> v;
  ckout << "Creating a new chare with a vector in it." 
	<< "with size " << msize << "." << endl;

  CProxy_TestChare test = CProxy_TestChare::ckNew(msize);
  ckout << "Chare created. Creating future and asking for the vector." << endl;
  CkFuture f = CkCreateFuture();
  test.getData(f);
  VarSizeMsg* m = (VarSizeMsg*)CkWaitFuture(f);
  m->fillVector(v);
  ckout << "Data recovered. It is:\n";
  for (int i = 0; i < v.size(); ++i) {
    ckout << v[i] << " ";
  }
  ckout << endl;
  ckout << "Now deleting the message." << endl;
  delete m;
  CkExit();
}

Driver::Driver(CkArgMsg* m) {
  thisProxy.run();
}

#include "mymessage.def.h"
